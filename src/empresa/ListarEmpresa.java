package empresa;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.DefaultListModel;
import javax.swing.JList;


public class ListarEmpresa {
    public void listar(JList ListaEmpresa) throws SQLException{
        ListaEmpresa.removeAll();
        DefaultListModel dfm = new DefaultListModel();
        Connection c = conexao.ObterConexao();
        String SQL= "select*from sc_empresa.dono";
        PreparedStatement ps = c.prepareStatement(SQL);
        ResultSet rs = ps.executeQuery(SQL);
        while(rs.next()){
            dfm.addElement(rs.getString("nome"));
        }
        ListaEmpresa.setModel(dfm);
        c.close();
    }
    public void listarFuncionario(JList ListaFuncionario) throws SQLException{
        ListaFuncionario.removeAll();
        DefaultListModel dfm = new DefaultListModel();
        Connection c = conexao.ObterConexao();
        String SQL= "select*from sc_empresa.funcionario";
        //PreparedStatement ps = c.prepareStatement(SQL);
        Statement ps = c.createStatement();
        ResultSet rs = ps.executeQuery(SQL);
        while(rs.next()){
            dfm.addElement(rs.getString("nome"));
        }
        ListaFuncionario.setModel(dfm);
        c.close();
    }
}
