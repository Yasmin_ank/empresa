/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package empresa;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author aluno
 */
public class insercao {
    
    public void inserirCliente (String nome, String dataNasc, String cpf, int numeroFun){
        try {
            Connection c = conexao.ObterConexao();
            String SQL = "insert into sc_empresa.cliente(nome, cpfCliente,numeroFun, dataNasc) values(?,?,?,?); ";
            PreparedStatement s = c.prepareStatement(SQL);
            s.setString(1, nome);
            s.setString(2, dataNasc);
            s.setString (3, cpf);
            s.setInt(4, numeroFun);
            c.close();
        } catch (SQLException ex) {
            Logger.getLogger(insercao.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    } 
    public void inserir (String cpf, String cnpj, String nomeEmpresa, String numeroFuncionarios){
        try {
            Connection c = conexao.ObterConexao();
            PreparedStatement ps= c.prepareStatement("insert into sc_empresa.dono(cpf, cnpj, nomeEmpresa, numeroFuncionarios) values (?,?,?,?)");
            ps.setString(1, cpf);
            ps.setString(2, cnpj);
            ps.setString(3, nomeEmpresa);
            ps.setInt(4, Integer.valueOf(numeroFuncionarios));
            ps.executeUpdate();
            c.close();
        } catch (SQLException ex) {
            Logger.getLogger(insercao.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    public void inserirFuncionario (String nome, String CPF, String endereco, String numeroContrato, String numeroDepartamento){
        try {
            Connection c = conexao.ObterConexao();
            PreparedStatement ps= c.prepareStatement("insert into sc_empresa.funcionario(nome,cpf,endereco, numerocontrato, numerodepartamento) values (?,?,?,?,?)");
            ps.setString(1, nome);
            ps.setString(2, CPF);
            ps.setString(3, endereco);
            ps.setInt(4, Integer.valueOf( numeroContrato) );
            ps.setInt(5, Integer.valueOf(numeroDepartamento));
            ps.executeUpdate();
            c.close();
        } catch (SQLException ex) {
            Logger.getLogger(insercao.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
}
