
package empresa;


public class Funcionario {
    private int numeroDepartamento;
    private String nome;

    public Funcionario(int numeroDepartamento, String nome) {
        this.numeroDepartamento = numeroDepartamento;
        this.nome = nome;
    }

    public int getNumeroDepartamento() {
        return numeroDepartamento;
    }

    public String getNome() {
        return nome;
    }
    
    
}
