
package empresa;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;

public class ListarFuncionario {
  public void  selecaoDepart(JComboBox combo){
      try {
          DefaultComboBoxModel m = new DefaultComboBoxModel();
          Connection c = new conexao.ObterConexao();
          Prepared Statement p = c.preparedStatement("Select * from sc_empresa.funcionario");
          ResultSet rs = p.executeQuery();
          while(rs.next()){
              Funcionario a = new Funcionario (rs.getInt("departamento"), m.addElement(a), rs.getString("nome"));
          }
          combo.setModel(m);
      } catch (SQLException ex) {
          Logger.getLogger(ListarFuncionario.class.getName()).log(Level.SEVERE, null, ex);
      }
  }  
}
