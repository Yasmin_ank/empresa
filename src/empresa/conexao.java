/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package empresa;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author aluno
 */
public class conexao {
     private static final String DRIVER ="org.postgresql.Driver";
     private static final String URL="jdbc:postgresql://10.90.24.54/yasmin_ank";
     private static final String USER = "yasmin_ank";
     private static final String PASS= "yasmin_ank";
     
    /**
     *
     * @return
     */
    public static Connection ObterConexao(){
         try {
             Class.forName(DRIVER);
             try {
                 return DriverManager.getConnection(URL, USER, PASS);
             } catch (SQLException ex) {
                 Logger.getLogger(conexao.class.getName()).log(Level.SEVERE, null, ex);
             }
         } catch (ClassNotFoundException ex) {
             Logger.getLogger(conexao.class.getName()).log(Level.SEVERE, null, ex);
         }
         return null;

    }
     
     
     public static Connection getConnection()throws SQLException, ClassNotFoundException{
         String url= URL;
         String usuario = USER;
         String senha = PASS;
         
         Connection c;
         Class.forName(DRIVER);
         c = DriverManager.getConnection(url,usuario,senha);
         return c;
     }
     
     public static void closeConnection(Connection con){
         if(con!=null){
             try {
                 con.close();
             } catch (SQLException ex) {
                 Logger.getLogger(conexao.class.getName()).log(Level.SEVERE, null, ex);
             }
         }
         
     }
     
     public static void closeConnection(Connection con, PreparedStatement stmt){
         closeConnection(con);
         if(stmt != null){
             try {
                 stmt.close();
             } catch (SQLException ex) {
                 Logger.getLogger(conexao.class.getName()).log(Level.SEVERE, null, ex);
             }
         }
     }
     
     public static void closeConnection(Connection con, PreparedStatement stmt, ResultSet rs){
         closeConnection(con,stmt);
         if(rs!=null){
             try {
                 rs.close();
             } catch (SQLException ex) {
                 Logger.getLogger(conexao.class.getName()).log(Level.SEVERE, null, ex);
             }
         }
     }
     
    
}
